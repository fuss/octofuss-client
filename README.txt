To build a new package:

* Bump the version *both* in debian/changelog *and* setup.py
* Build the package as documented on
  https://fuss-dev-guide.readthedocs.io/it/latest/pacchetti-e-repository.html
  using debian/rules debsrc to create the source tarball.
