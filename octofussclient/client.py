from twisted.internet import task, reactor
from twisted.python import reflect

import os
import grp
import tempfile
import stat
import time
import sys
from xmlrpc import client as xmlrpc_client
import configparser
from optparse import OptionParser
from subprocess import Popen, PIPE
import logging
from logging.handlers import SysLogHandler
# our modules
from . import helpers
from octofuss import helpers as ofhelpers
import random
import dbus, dbus.service
from . import clientdata

FUSS_CLIENT_CONF = "/etc/fuss-client/server.conf"
OCTOFUSS_CLIENT_CONF = "/etc/octofuss-client/server.conf"

class Parser(OptionParser):
    def __init__(self, *args, **kwargs):
        OptionParser.__init__(self, *args, **kwargs)

    def error(self, msg):
        sys.stderr.write("%s: error: %s\n\n" % (self.get_prog_name(), msg))
        self.print_help(sys.stderr)
        sys.exit(2)

AT_BOOT_AFTER = 5
LOOP_EVERY = 60*5

class OctofussClient(object):
    package_command = '/usr/bin/apt-get'

    def __del__(self):
        if self.looping:
            self.looping.stop()

    def __init__(self, **kw):
        self.options = kw
        self.log = logging.getLogger("octofuss.client")
        self.looping = None
        self.DRY_RUN = False
        self.CONF_FILE = None
        self.MYFQDN = helpers.get_fqdn()
        self.MYNAME = helpers.get_hostname()

        if 'interactive' not in self.options.keys():
            from .dbussrv import OctofussClientDBus
            bus = dbus.SystemBus()
            name = dbus.service.BusName('org.octofuss.OctofussClient', bus=bus)
            d = OctofussClientDBus(name, '/')
            reactor.callLater(AT_BOOT_AFTER, self.at_boot_actions)
            self.looping = task.LoopingCall(self.loop_actions)
            self.looping.start(LOOP_EVERY)
            logging.basicConfig(level=logging.DEBUG,
                                format='%(levelname)s %(message)s')

            syslog = SysLogHandler()
            syslog.setLevel(logging.DEBUG)
            self.log = logging.getLogger('')
            self.log.addHandler(syslog)
        else:
            parser = Parser(usage="usage: %prog [options]",
                            version="%prog ",
                            description="Connect to an octofuss server to exchange data")
            parser.add_option("-j", "--joinserver", metavar="CONFSERVERHOST", help="update configuration to use the given server")
            parser.add_option("-g", "--groupjoin", metavar="CONFSERVERGROUP", help="inform the server to which computers group we want to belong to")
            parser.add_option("-s", "--useserver", metavar="SERVERHOST", help="connect now to this host")
            parser.add_option("-c", "--conffile", metavar="CONFFILE", help="use the specified config file")
            parser.add_option("-l", "--listgroups", action="store_true", help="list available clusters/groups on server")
            parser.add_option("-v", "--verbose", action="store_true", help="verbose output")
            parser.add_option("--debug", action="store_true", help="debug output")
            parser.add_option("--syslog", action="store_true", help="send loggin output to syslog")
            parser.add_option("--dryrun", action="store_true", help="simulate actions")
            self.options, self.args = parser.parse_args()
            options = self.options
            args = self.args

            # log configuration
            if options.dryrun:
                self.DRY_RUN = True

            if options.verbose:
                logLevel = logging.INFO

            elif options.debug:
                logLevel = logging.DEBUG
            else:
                logLevel = logging.WARNING

            if options.syslog:
                logging.basicConfig(level=logLevel, format='%(levelname)s %(message)s')
                syslog = SysLogHandler()
                syslog.setLevel(logLevel)
                logging.getLogger('').addHandler(syslog)
            else:
                logging.basicConfig(level=logLevel, format='%(levelname)s %(message)s')

            self.log = logging.getLogger("octofuss.client")

            if options.conffile:
                self.CONF_FILE = options.conffile

            self.read_conf()

            if options.useserver and options.joinserver:
                self.log.warn("Can't use useserver and joinserver together!")
                sys.exit(3)

            if options.joinserver:
                self.conf_set_server(options.joinserver)
                self.save_conf()

                if options.groupjoin:
                    join_group = options.groupjoin
                    log.info("Attempting to join group %s" % join_group)
                else:
                    join_group = ""

                    self.join_group(join_group)

            if options.useserver:
                self.server_hostname = options.useserver
            else:
                try:
                    self.server_hostname = self.conf.get("Server","address")
                except Exception as e:
                    self.log.error("%s (configuration file)" % e)

            if not self.server_hostname:
                self.log.error("Not running: no server configured. Exiting")
                sys.exit(0)

            if options.listgroups:
                tmp_server_url = 'http://%s:13400/octofuss' % self.server_hostname
                try:
                    o = "Available clusters/group on server:\n\n"
                    groups = ofhelpers.list_cluster(tmp_server_url)
                    print("\n".join(groups))
                except:
                    self.log.error("Error getting group list from server")
                sys.exit(0)
            self.at_boot_actions()
            # run once
            self.loop_actions()

    def read_conf(self):
        if self.CONF_FILE:
            pass
        # try to lookup some configurations
        elif os.path.isfile(FUSS_CLIENT_CONF):
            self.CONF_FILE = FUSS_CLIENT_CONF
        elif os.path.isfile(OCTOFUSS_CLIENT_CONF):
            self.CONF_FILE = OCTOFUSS_CLIENT_CONF
        else:
            self.CONF_FILE = "server.conf"
        self.log.info("USING CONF FILE {}".format(self.CONF_FILE))
        self.conf = configparser.ConfigParser()
        self.conf.read(self.CONF_FILE)

        if not self.conf.get("Server", "address"):
            if os.path.isfile("/etc/fuss-server/fuss-server.yaml"):
                self.conf.read_string("[Server]\naddress = 127.0.0.1")

        self.log.info(self.conf.get("Server", "address"))

    def join_group(self, group):
        if not self.DRY_RUN:
            self.call('client_join', self.MYNAME, group)
            if group:
                self.log.info("Successfully added client %s to group %s" % (self.MYFQDN, group))
            else:
                self.log.info("Successfully added client %s without a specific group" % self.MYFQDN)

    def conf_set_server(self, server):
        if self.conf:
            if not self.conf.has_section("Server"):
                if not self.DRY_RUN:
                    self.conf.add_section("Server")
            self.conf.set("Server", "address", server)


    def save_conf(self):
        self.log.info("Updating configuration")
        if not self.DRY_RUN:
            try:
                self.conf.write(open(self.CONF_FILE, "w"))
            except Exception as e:
                self.log.error(e)


    @property
    def server_address(self):
        self.read_conf()
        try:
            self.server_hostname = self.conf.get("Server","address")
            if self.server_hostname:
                return True
            else:
                return False
        except:
            return False

    @property
    def server(self):
        server = xmlrpc_client.ServerProxy('http://%s:13400/octofuss' % self.server_hostname,
                                           xmlrpc_client.Transport(),
                                           "UTF-8",
                                           allow_none=True,
                                           )
        return server


    def run_prefixed_methods(self, prefix):
        methods = reflect.prefixedMethodNames(OctofussClient, prefix)
        for m in methods:
            f = getattr(self, prefix+m)
            try:
                f()
            except Exception as e:
                self.log.warn(str(e))

    def at_boot_actions(self):
        if not self.server_address:
            return
        prefix = "boot_action_"
        self.run_prefixed_methods(prefix)

    def loop_actions(self):
        t = self.server_address
        if not t:
            return

        prefix = "action_"
        self.run_prefixed_methods(prefix)

    def call(self, method, *args, **kw):
        try:
            r = eval("self.server.%s(*args)" % method)
            return r
        except Exception as e:
            self.log.error("{}".format(e))
            return None

        return None

    #helpers

    def do_aptget_update(self):
        self.log.debug("Updating package list")
        exit_code, output = helpers.run_annotated("apt-get update")
        return exit_code, output


    # interactions

    def boot_action_client_data(self):
        try:
            self.call('client_components_fqdn', self.MYFQDN, clientdata.get_data())
        except Exception as e:
            self.log.critical("clientdata.get_data() raised %s. OCTOFUSS-CLIENT IS NOT WORKING!!!" % e)
            pass

    def boot_action_ping_server(self):
        self.log.info("Trying to contact Octofuss server")
        self.call('client_ping_fqdn', self.MYNAME)

    def action_sync_privileges(self):
        self.log.info("Syncing privileges")
        try:
            privileges = self.call('privileges')
            if not privileges:
                mesg = "No privilege got from server"
                self.log.debug(mesg)
                return
            mesg = "Got privileges from server"
            self.log.debug(mesg)
            self.log.debug(privileges)
            current={}
            absent=[]
            for i in privileges.keys():
                try:
                    members=grp.getgrnam(i)[3]
                    current[i]=members
                except:
                    self.log.debug("group " + i + " not used here")
                    absent.append(i)
            for i in absent:
                del privileges[i]
            to_add={}
            to_del={}
            added=[]
            removed=[]
            for i in privileges.keys():
                added=list(set(privileges[i]) - set(current[i]))
                if added:
                    to_add[i]=added
                removed=list(set(current[i]) - set(privileges[i]))
                if removed:
                    to_del[i]=removed
            for i in to_add.keys():
                [ os.system("adduser {} {}".format(j,i)) for j in to_add[i] ]
                self.log.info('groups modified addition')
                self.log.info(to_add)
            for i in to_del.keys():
                [ os.system("gpasswd -d {} {}".format(j,i)) for j in to_del[i] ]
                self.log.info('groups modified deletion')
                self.log.info(to_del)
            if to_add or to_del:
                os.system("nscd -i group")
                if os.path.exists("/etc/fuss-server"):
                    os.system("pkill ext_unix_group")

        except Exception as e:
            self.log.error("Error syncing privileges: %s" % e)

    def action_flush_queues(self):
        from .dbussrv import message_queue
        while not message_queue.empty():
            message = message_queue.get()
            self.call('user_request', str(message[0]),
                      str(message[1]), str(message[2]))

    def action_get_upgrades(self):
        self.log.debug("Getting upgrades for %s" % self.MYNAME)
        upgrades = self.call('get_upgrade_todo', self.MYNAME)
        if not upgrades:
            self.log.info("No upgrades to be managed")
            return

        self.log.info("%d upgrades to be managed" % len(upgrades))

        up_exit, up_output = self.do_aptget_update()

        for up in upgrades:
            if up_exit > 0:
                self.log.info("Not executing upgrade named '%s'" % up['name'])
                res_dict = dict(name=up['name'],
                                output=up_output,
                                result=up_exit,
                                ended=time.time())
            else:
                self.log.info("Executing upgrade named '%s'" % up['name'])

                if self.DRY_RUN:
                    exit_code, output = (0, "OK!")
                else:
                    exit_code, output = helpers.run_annotated("%s -y --force-yes  --allow-unauthenticated %s 2>&1" % (self.package_command, up['type']))


                res_dict = dict(name=up['name'],
                                output=xmlrpc_client.Binary(bytes(output, encoding='utf-8')),
                                result=exit_code,
                                ended=time.time())

            self.log.debug("Sending data back to server")
            self.log.debug(str(res_dict))
            if self.DRY_RUN:
                r = "ACK"
            else:
                r = self.call('post_upgrade_results', self.MYNAME, res_dict)

            self.log.debug("Server returned: %s" % r)
            self.log.info("Ended upgrade named '%s'" % up['name'])

    def action_get_printers(self):
        self.log.debug("Getting printers")
        printers = self.call('client_printers', self.MYNAME)
        if not printers:
            self.log.info("No printers to configure")
            # refs #12376 #566
            # Comment this return, because if there are printers to be removed in this client, but
            # octofussd does not have printers in its database, the code will exit *now*
            # return
        # Mitigate error object of type 'NoneType' has no len
        else:
            self.log.info("{} printer(s) to be configured".format(len(printers)))
            self.log.debug("Printers from server:" + repr(printers))
        current_printers = []
        # refs #12376 #566
        # We need also the urls, not only the queue names
        current_printers_urls = {}
        try:
            p = Popen(["/usr/bin/lpstat", "-v"], stdout=PIPE).communicate()[0]
            p = p.decode('utf-8').strip()
            if p:
                for line in p.split("\n"):
                    # refs #12376 #566
                    # Save the queue names *and* the urls
                    queue = line.split(" ")[2][:-1]
                    url = line.split(" ")[3]
                    current_printers.append(queue)
                    current_printers_urls[queue] = url
        except:
            self.log.warn("Error getting current printers")
            return

        to_configure = list(set(printers) - set(current_printers))
        if not to_configure:
            self.log.info("All printers already configured")

        self.log.info("Printers to configure:" + repr(to_configure))
        for queue in to_configure:
            cmdline = "lpadmin -p {queue} -E -v ipps://{server}:631/printers/{queue}".format(queue=queue, server=self.server_hostname)
            os.system(cmdline)

        # refs #12376 #566
        to_remove = list(set(current_printers) - set(printers))
        server_url = "ipps://{server}".format(server=self.conf.get("Server", "address"))
        self.log.debug("Current printers:" + repr(current_printers))
        self.log.debug("Printers to remove:" + repr(to_remove))
        self.log.debug("Server URL:" + server_url)
        for queue in to_remove:
            if queue in current_printers_urls and server_url in current_printers_urls.get(queue, ""):
                # refs #12376 #566
                # Take a look at the step 3 in this comment: https://labs.truelite.it/issues/12376#note-4
                # Remove the printer here on the client only if the url is in the form
                # ipps://ser.ver.ip/something
                self.log.info("Removing queue '{queue}'".format(queue=queue))
                cmdline = "lpadmin -x {queue}".format(queue=queue)
                os.system(cmdline)
            else:
                self.log.info("Queue '{queue}' is not to be removed".format(queue=queue))

    def action_get_scripts(self):
        self.log.debug("Getting scripts")
        scripts = self.call('get_script_todo', self.MYNAME)
        if not scripts:
            self.log.info("No scripts to be executed")
            return

        self.log.info("%d scripts to be executed" % len(scripts))
        for sc in scripts:
            self.log.debug('Data received for %s: %s' % (sc['name'], str(sc)))
            self.log.info("Executing script named '%s'" % sc['name'])

            if self.DRY_RUN:
                exit_code = 0
                output = "FooBar"*3
            else:
                fname = tempfile.mktemp()
                fp = open(fname, "w")
                fp.write(sc['script'])
                fp.close()
                os.chmod(fname, stat.S_IRUSR|stat.S_IWUSR|stat.S_IXUSR)
                exit_code, output = helpers.run_annotated(fname)
                if exit_code == None:
                    exit_code = 0
                os.unlink(fname)

            res_dict=dict(script=sc['scriptname'],name=sc['name'], result=exit_code,
                          output=xmlrpc_client.Binary(bytes(output, encoding="utf-8")), ended=time.time())
            self.log.debug("Sending data back to server")
            self.log.debug(str(res_dict))

            if self.DRY_RUN:
                r = "ACK"
            else:
                r = self.call('post_script_results', self.MYNAME, res_dict)

            self.log.debug("Server returned: %s" % r)
            self.log.info("Ended script named '%s'" % sc['name'])


    def action_get_packages(self):

        commandline = "%s -y --force-yes  --allow-unauthenticated install %s"

        self.log.info("Starting remote install check...")
        try:
            packages = self.call('install_queue', self.MYNAME)
            if not packages:
                self.log.info("No packages")
                return
            up_exit,up_output = self.do_aptget_update()
            if up_exit > 0:
                self.log.debug("Repository update error: %d" % up_exit)
                return

            mesg = "New software to be installed: "
            if len(packages) > 0:
                for i in packages:
                    mesg = mesg + i + " "
            else:
                mesg = "No new software to be installed"

            self.log.info(mesg)

            for package in packages:
                if up_exit > 0:
                    self.log.error("Not Installing %s - repository update error", package)
                    if self.DRY_RUN:
                        ris = "ack"
                    else:
                        ris = self.call('install_log', self.MYNAME,package,up_exit,up_output)
                    continue

                self.log.info("Installing: %s", package)
                if self.DRY_RUN:
                    res,logdata = random.randint(0,100), "FooBar"*3
                else:
                    res, logdata = helpers.run_annotated(commandline % (self.package_command, package))

                if res > 0:
                    self.log.error("Error installing: %s", package)

                if self.DRY_RUN:
                    ris = "ack"
                else:
                    ris = self.call('install_log', self.MYNAME,package,res,logdata)

                while ris != "ack":
                    self.log.info("Got %s: retrying..", ris)
                self.log.info("Results sent back to central server")
        except Exception as e:
            self.log.error(": %s", e)
