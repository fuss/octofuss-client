import os
import os.path
import yaml
import math
import apt
import psutil
import netifaces
import socket
import logging
from octofussclient import etcinfo


global log, IS_FUSS_SERVER
log = logging.getLogger("octofuss.client")
IS_FUSS_SERVER = False


def get_dmidata(log, IS_FUSS_SERVER):
    log.debug("Parsing dmidecode")
    p = os.popen("dmidecode")
    data = p.read()
    r = p.close()
    if r:
        return {}

    items = data.split("\n\n")
    items = items[1:]

    def parse_items(i):
        it = i.split("\n")
        it = it[2:]
        voices = {}
        for x in it:
            s = x.strip().split(":")
            if len(s) > 1:
                k = s[0].strip()
                v = s[1].strip()
            voices[k] = v
        return voices

    data = {"system": parse_items(items[1]),
            "board": parse_items(items[2]),
            "chassis": parse_items(items[3])}

    return data

dmi_interesting = [ ('chassis','Type',"chassis_type"),
                    ('system','Manufacturer', "system_vendor"),
                    ('system','Serial Number', "serial_serial"),
                    ('system','Product Name', "system_product")
                    ]

def human_size(number):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    human = number
    scale = 0
    if number != 0:
        scale = int((math.log10(number)) / 3)
        scale = min(scale, len(suffixes) - 1)
        human = number / (1024.0 ** scale)
    f = ('%.2f' % human).rstrip('0').rstrip('.')
    return "{} {}".format(f, suffixes[scale])


def get_is_server(log, IS_FUSS_SERVER):
    client_data = {}
    client_data["is_server"] = "no"
    FUSS_SERVER_CONF = "/etc/fuss-server/fuss-server.yaml"
    if IS_FUSS_SERVER:
        client_data["is_server"] = "yes"
        if os.path.exists("/etc/fuss-server/fuss-captive-portal.conf"):
            log.debug("This FUSS Server has a configured captive portal")
            client_data["has_captive_portal"] = "yes"
        else:
            log.debug("This FUSS Server doesn't have any configured captive portal")
            client_data["has_captive_portal"] = "no"

        # total number of users
        p = os.popen("ldapsearch -x -LLL uid=* uid", "r")
        data = p.readlines()
        counter = 0
        for line in data:
            if line.startswith("dn: "):
                if "uid=admin" in line or "uid=nobody" in line:
                    continue
                counter += 1
        client_data['total_users'] = counter
        log.debug("%s total users in the system" % client_data["total_users"])

        # Uptime
        p = os.popen("uptime -p", "r")
        data = p.read().strip()
        client_data['uptime'] = data
        log.debug("Uptime %s" % client_data["uptime"])
    return client_data


def get_video_size(log, IS_FUSS_SERVER):
    client_data = {}
    log.debug("Collecting video size info")
    try:
        video_size = open('/var/tmp/fuss-display-size').read().strip()
        client_data['video_size'] = video_size
        log.debug("Video size info collected")
    except:
        log.debug("Error collecting video size, skipping.")
        pass
    return client_data


def get_interfaces(log, IS_FUSS_SERVER):
    client_data = {}
    FUSS_SERVER_CONF = "/etc/fuss-server/fuss-server.yaml"
    wan_iface = None
    if IS_FUSS_SERVER:
        log.debug("Searching wan on fuss server")
        fuss_server_conf = yaml.load(open(FUSS_SERVER_CONF))
        wan_iface = fuss_server_conf.get("external_ifaces", None)
        if wan_iface:
            wan_iface = wan_iface[0]
            log.debug("Found wan %s" % wan_iface)

    for iface in netifaces.interfaces():
        if iface == "lo":
            continue

        iface_conf = netifaces.ifaddresses(iface)
        if iface_conf.get(socket.AF_INET):
            for conf in iface_conf.get(socket.AF_INET):
                if "addr" in conf:
                    if wan_iface and iface == wan_iface:
                        client_data['external_iface'] = str(iface)
                        client_data['external_ip_address'] = conf['addr']
                        log.debug("Found wan iface %s with ip %s" % (iface, conf["addr"]))
                    else:
                        client_data['iface {}'.format(iface)] = conf['addr']
                        log.debug("Found iface %s with ip %s" % (iface, conf["addr"]))
                try:
                    mac = iface_conf[netifaces.AF_LINK][0]["addr"]
                    client_data['mac {}'.format(iface)] = mac
                    log.debug("Found MAC %s on iface %s" % (mac, iface))
                except:
                    log.debug("No MAC for %s" % iface)
    return client_data


def get_installed_packages(log, IS_FUSS_SERVER):
    client_data = {}
    # N. of installed packages
    p = os.popen("dpkg -l | wc -l", "r")
    client_data['ninstalledpackages'] = int(p.read()) - 5
    log.debug("%s installed packages" % client_data["ninstalledpackages"])

    # fuss-* version
    log.debug("Parse packages version")
    pkg_cache = apt.Cache()
    checked_packages = [
        'fuss-client',
        'fuss-artwork',
        'fuss-utility',
        'fuss-server',
        'fuss-backup',
        'octofuss-client',
        'octomon-sender',
        'octofussd',
        'octonet',
        'libreoffice',
        'libreoffice-base',
        'libreoffice-writer',
        'libreoffice-calc',
        'libreoffice-impress',
        'firefox-esr',
        'chromium',
        'ansible',
        'unattended-upgrades',
        'coova-chilli',
        'systemd',
    ]
    for package in checked_packages:
        if pkg_cache.has_key(package):
            found_package = pkg_cache[package]
            if found_package.is_installed:
                client_data[package] = found_package.installed.version
                log.debug("%s %s" % (package, found_package.installed.version))

    # refs #617 : rename fuss-client into fussclient to ensure correct parsing in octomon
    if "fuss-client" in client_data:
        log.debug("Renaming fuss-client into fussclient")
        client_data["fussclient"] = client_data["fuss-client"]
        del client_data["fuss-client"]
    return client_data


def get_ram(log, IS_FUSS_SERVER):
    client_data = {}
    # total amount of ram memory
    client_data['totalram'] = human_size(psutil.virtual_memory().total)
    log.debug("%s of RAM" % client_data["totalram"])
    if IS_FUSS_SERVER:
        client_data['availableram'] = human_size(psutil.virtual_memory().available)
        log.debug("%s of free RAM" % client_data["availableram"])
    return client_data


def get_disk_usage(log, IS_FUSS_SERVER):
    client_data = {}
    # disk usage for every physical filesystems
    log.debug("Parsing disks info")
    for part in psutil.disk_partitions():
        device = part.device
        mpoint = part.mountpoint
        if not device or mpoint in ["/tmp", "/var/tmp"] or mpoint.startswith("/media"):
            continue
        log.debug("Parsing %s mounted on %s" % (device, mpoint))
        usage = psutil.disk_usage(mpoint)
        usage_percent = usage.percent
        total_size = human_size(usage.total)
        client_data["{} usage".format(mpoint)] = "{}%".format(usage_percent)
        client_data["{} size".format(device)] = total_size
        # refs #632
        client_data["{} mountpoint".format(device)] = mpoint
        client_data["{} fstype".format(device)] = part.fstype
    return client_data


def get_cpu(log, IS_FUSS_SERVER):
    client_data = {}
    # cpu type
    p = os.popen("grep 'model name' /proc/cpuinfo | uniq | cut -d ':' -f 2",'r')
    data = p.read().strip()
    client_data['cputype'] = data
    log.debug("CPU: %s" % data)

    # cpu speed
    p = os.popen("grep 'cpu MHz' /proc/cpuinfo | head -1 | cut -d ':' -f 2",'r')
    data = p.read().strip()
    client_data['cpuspeed'] = data
    log.debug("CPUSPEED: %s" % data)
    return client_data


def get_videocard(log, IS_FUSS_SERVER):
    client_data = {}
    # video card
    p = os.popen("lspci | grep VGA",'r')
    data = p.read().strip()
    client_data['videocard'] = data[35:]
    log.debug("Videocard %s" % client_data["videocard"])
    return client_data


def parse_dmi(log, IS_FUSS_SERVER):
    client_data = {}
    dmidata = get_dmidata(log, IS_FUSS_SERVER)
    # collect data from DMI
    for k in dmi_interesting:
        if k[0] in dmidata.keys():
            if k[1] in dmidata[k[0]].keys():
                client_data[k[2]] = dmidata[k[0]][k[1]]
    return client_data


def get_fqdn(log, IS_FUSS_SERVER):
    client_data = {}
    client_data["fqdn"] = socket.getfqdn()
    return client_data


def get_octopyversion(log, IS_FUSS_SERVER):
    try:
        import pkg_resources
        ver = pkg_resources.get_distribution("octofussclient").version
    except:
        ver = "UNKNOWN"
    return {"__version__": ver}


def get_data():
    global log
    FUSS_SERVER_CONF = "/etc/fuss-server/fuss-server.yaml"
    IS_FUSS_SERVER = False
    if os.path.exists(FUSS_SERVER_CONF):
        log.debug("This is a fuss server")
        IS_FUSS_SERVER = True
    client_data = {}
    FUNCTIONS = [
        get_is_server,
        get_video_size,
        get_interfaces,
        get_installed_packages,
        get_ram,
        get_disk_usage,
        get_cpu,
        get_videocard,
        parse_dmi,
        get_fqdn,
        get_octopyversion,
        etcinfo.etc_info
    ]

    for f in FUNCTIONS:
        try:
            this = f(log, IS_FUSS_SERVER)
            for i in this.keys():
                if i in client_data:
                    log.warning("%s will be overwritten by the value from %s" %(i, f.__name__))
                client_data[i] = this[i]
        except Exception as e:
            log.warning("%s raised %s during execution" %(f.__name__, e))

    return client_data
