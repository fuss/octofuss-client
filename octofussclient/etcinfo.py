# Copyright (c) 2019 Marco Marinello <mmarinello@fuss.bz.it>
# This portion of code gathers info from etckeeper about
# latest modifications on the server
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import git
import json
import logging


def etc_info(log, IS_FUSS_SERVER):
    if not IS_FUSS_SERVER:
        log.debug("This is not a server, no data gathering")
        return {}
    if not os.path.exists("/etc/.git"):
        log.debug("/etc does not contain a valid git archive")
        return {}
    # Initialize git object
    etc = git.Repo("/etc")
    commits = list(etc.iter_commits())
    if len(commits) == 0:
        log.debug("No commits in /etc")
        return {}
    # Pick just the latest 10 commits
    commits = commits[:10]
    # Output will be a json list of dicts
    # [ COMMIT {message: , datetime: , additions: , deletions: }]
    out = []
    for i in commits:
        this = {"additions": [], "deletions": []}
        parent = i.parents[0] if i.parents else None
        this["message"] = i.message.strip()
        this["datetime"] = i.authored_datetime.strftime("%d/%m/%Y %H:%M:%S")
        for ob in i.diff(parent):
            if ob.a_blob:
                this["additions"].append(ob.a_blob.path)
            if ob.b_blob:
                this["deletions"].append(ob.b_blob.path)
        out.append(this)
    txt = json.dumps(out)
    # Since Piccardi thinks that increasing the character limit of a DB column
    # is too laborious (https://work.fuss.bz.it/issues/778),
    # divide the string every 199 characters
    output = {}
    splitted = [txt[i:i+199] for i in range(0, len(txt), 199)]
    output["etc_segments"] = len(splitted)
    n = 0
    for i in splitted:
        output["etc_segment%s" % n] = i
        n += 1
    return output


# Just to test
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    print(etc_info(logging, True))
