import subprocess, os
import fcntl
import select
import socket


def get_fqdn():
    return os.popen("hostname --fqdn").read().strip()

def get_hostname():
    return socket.gethostname()

def simple_popen(cmdline):
    return subprocess.Popen(cmdline,
                            stdin=open("/dev/null", "r"),
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            close_fds=True,
                            shell=True,
                            cwd="/")

def stream_output(stdout, stderr):
    """
    Take two file descriptors and generate their output, with stdout lines
    prefixed by "I:", stderr lines prefixed by "E:".

    Once both file descriptors reach EOF, the function stops.
    """
    fds = [stdout, stderr]
    bufs = ["", ""]
    prefixes = ["I:", "E:"]
    # Set both pipes as non-blocking
    for fd in fds:
        fcntl.fcntl(fd, fcntl.F_SETFL, os.O_NONBLOCK)
    # Multiplex stdout and stderr with different prefixes
    while len(fds) > 0:
        s = select.select(fds, (), ())
        for fd in s[0]:
            idx = fds.index(fd)
            buf = fd.read().decode()
            if len(buf) == 0:
                fds.pop(idx)
                if len(bufs[idx]) != 0:
                    r = "{prefix} {payload}\n".format(prefix=prefixes[idx], payload=bufs.pop(idx))
                    yield r
                    prefixes.pop(idx)
            else:
                bufs[idx] += str(buf)
                lines = bufs[idx].split("\n")
                bufs[idx] = lines.pop()
                for l in lines:
                    r = "{prefix} {payload}\n".format(prefix=prefixes[idx], payload=l)
                    yield r

def run_annotated(cmdline, debug=False):
    """
    Run the command given in cmdline, returning the tuple (res, output)
    where res is the return code and output is the annotated stdout+stderr
    """

    proc = simple_popen(cmdline)
    if debug:
        output = []
        for x in stream_output(proc.stdout, proc.stderr):
            output.append(x)
    else:
        output = [x for x in stream_output(proc.stdout, proc.stderr)]

    exit_code = proc.wait()
    return exit_code, "".join(output)
