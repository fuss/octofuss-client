
import dbus, dbus.glib, dbus.service
import queue

message_queue = queue.Queue()

class OctofussClientDBus(dbus.service.Object):
    @dbus.service.method(dbus_interface='org.octofuss.Interface',
                        in_signature='sss', out_signature='b')
    def user_question(self, user, host, question):
        message_queue.put([user, host, question])
        return True
