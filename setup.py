#!/usr/bin/env python3

from distutils.core import setup

setup(name='OctofussClient',
      version='10.0.6',
      description='Octofuss Client',
      author='FUSS Package team',
      author_email='packages@fuss.bz.it',
      url='https://gitlab.fuss.bz.it/fuss/octofuss-client',
      scripts=['octofuss-client'],
      packages=['octofussclient'],
      data_files=[('/etc/dbus-1/system.d/', ['octofuss-client-dbus.conf'])],

      )
